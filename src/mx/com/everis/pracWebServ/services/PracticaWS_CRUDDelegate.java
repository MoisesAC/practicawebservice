package mx.com.everis.pracWebServ.services;

import javax.jws.WebService;


@WebService (targetNamespace="http://services.pracWebServ.everis.com.mx/", serviceName="PracticaWS_CRUDService", portName="PracticaWS_CRUDPort")
public class PracticaWS_CRUDDelegate{

    mx.com.everis.pracWebServ.services.PracticaWS_CRUD _practicaWS_CRUD = null;

    public float addValue (float value) {
        return _practicaWS_CRUD.addValue(value);
    }

    public float substractValue (float value) {
        return _practicaWS_CRUD.substractValue(value);
    }

    public PracticaWS_CRUDDelegate() {
        _practicaWS_CRUD = new mx.com.everis.pracWebServ.services.PracticaWS_CRUD(); }

}